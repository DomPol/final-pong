
#include "raylib.h"

#include <stdio.h>
#include <stdlib.h>

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = GAMEPLAY;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    player.y =screenHeight/2-25;
    player.x = 50;
    int playerSpeedY = 7;
    
    int rectangleWidth = 25;
    int rectangleHeight =50;
    
    Rectangle enemy;
    enemy.y=screenHeight/2-25;
    enemy.x= screenWidth-50;
    
    int enemySpeedY;
    
    Vector2 ballPosition;
    ballPosition.x= screenWidth/2;
    ballPosition.y= screenHeight/2;
    Vector2 ballSpeed;
    ballSpeed.x = 4;
    ballSpeed.y = 4;
    int ballRadius =25;
    
    int playerLife = 3;
    int enemyLife = 3;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    bool fadeOut = true;
    
    float alpha = 0;
    
    float fadeSpeed = 0.01f;
    
    bool pause =false ;
    char *text ;  
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Font PongFont = LoadFontEx("Resources/Park Lane NF.ttf", 100, 0, 250);
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if(fadeOut)
                {
                alpha += fadeSpeed;
            
                    if(alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        fadeOut = !fadeOut;
                    }
                }
                
                else
                {
                    alpha -= fadeSpeed;
                    if(alpha <= 0.0f)
                    {
                        alpha = 0.0f;
                        screen = TITLE;
                    }
                }
                
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed (KEY_ENTER))
                {
                    screen = GAMEPLAY;
                }
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                
                if (!pause) {
                ballPosition.x += ballSpeed.x;
                ballPosition.y += ballSpeed.y;
                }
                
                
        
                
                // TODO: Player movement logic.......................(0.2p)
                 if (!pause) {
                 if (player.y>0 && IsKeyDown(KEY_Q)){
        
                    player.y-=playerSpeedY;
                    }
                
                
                    if(player.y < screenHeight -50 && IsKeyDown(KEY_A)){
               
                    player.y+=playerSpeedY;
                    
                    } 
                 }        
                // TODO: Enemy movement logic (IA)...................(1p)
                
                
                if( ballPosition.x > screenWidth/2){
                if(ballPosition.y > enemy.y){
                enemy.y+=playerSpeedY;
                }
                
                if(ballPosition.y < enemy.y){
                enemy.y-=playerSpeedY;
                }
                }
        
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                 if (CheckCollisionCircleRec(ballPosition, ballRadius,
                (Rectangle){ player.x, player.y, rectangleWidth, rectangleHeight})){
                     if (ballSpeed.x < 0){
                     ballSpeed.x= -ballSpeed.x +1;
                    // PlaySound(fxWav1);
                     } 
                     
                }
                
           
                     
                  
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                 if (CheckCollisionCircleRec(ballPosition, ballRadius,
                 (Rectangle){ enemy.x, enemy.y, rectangleWidth, rectangleHeight})){
                    if (ballSpeed.x > 0){
                    ballSpeed.x= -ballSpeed.x-1;}
                   // PlaySound(fxWav1);
                    } 
                
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if (ballPosition.x >= screenWidth-ballRadius){
                    ballSpeed.x= -ballSpeed.x;
                  //  PlaySound(fxWav2);
                }
                if (ballPosition.x <= ballRadius){
                    ballSpeed.x= -ballSpeed.x;
                    //PlaySound(fxWav2);
                }
                
                if (ballPosition.y >= screenHeight-ballRadius){
                    ballSpeed.y= -ballSpeed.y-1;
                    //PlaySound(fxWav1);
                }
                
                if (ballPosition.y <= ballRadius ){
                    ballSpeed.y= -ballSpeed.y+1;
                    //PlaySound(fxWav1);
                }
                
                // TODO: Life bars decrease logic....................(1p)
                 if (ballPosition.x >= screenWidth- ballRadius) {
                    enemyLife--;
                    ballPosition.x= screenWidth/2;
                    ballPosition.y= screenHeight/2;
                    ballSpeed.x = 4;
                     ballSpeed.y = 4;
                    } 
            
                if (ballPosition.x <= ballRadius) {
                    playerLife--;
                    ballPosition.x= screenWidth/2;
                    ballPosition.y= screenHeight/2;
                     ballSpeed.x = 4;
                     ballSpeed.y = 4;
                } 
                
           
                
                  
                if (pause){
                    if (IsKeyPressed (KEY_R)){
                        enemyLife =3;
                        playerLife =3;
                        ballPosition.x=screenWidth/2;
                        ballPosition.y=screenHeight/2;
                        ballSpeed.x = 4;
                        ballSpeed.y = 4;
                        enemy.y=screenHeight/2-25;
                        player.y=screenHeight/2-25;
                        secondsCounter = 99;
                        pause = false;
                    }
                }
               

                // TODO: Time counter logic..........................(0.2p)
                if (!pause) {
                framesCounter++;
                
                if (framesCounter >= 60){
                secondsCounter--;
                framesCounter = 0;
                }
                }
                

                // TODO: Game ending logic...........................(0.2p)
                if (playerLife == 0){
                    gameResult = 0;
                    screen=ENDING;
                    
                }
                
                if (enemyLife == 0){
                    gameResult = 1;
                    screen=ENDING;
                    
                }
                
                if (secondsCounter == 0){
                        playerLife = 0;
                        screen= ENDING;
                    }
                
                // TODO: Pause button logic..........................(0.2p)
                 if (IsKeyPressed (KEY_SPACE)){
                 pause = !pause; }
               // if(pause){PlaySound(fxWav3);}
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
                if (IsKeyPressed (KEY_R)){
                    screen = GAMEPLAY;
                    playerLife = 3;
                    enemyLife = 3;
                    secondsCounter = 99;
                    gameResult = -1;
                }
                
                if (IsKeyPressed (KEY_SPACE)){
                    screen = TITLE;
                    playerLife = 3;
                    enemyLife = 3;
                    secondsCounter = 99;
                    gameResult = -1;
                }
                
               
               
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTextEx(PongFont, "Final Pong", (Vector2){screenWidth/2, screenHeight/2}, 50, 0, Fade(RED, alpha));
                    
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    ClearBackground(DARKBLUE);
                    
                    DrawTextEx(PongFont, "Welcome to Final Pong", (Vector2){screenWidth/4, screenHeight/4}, 50, 0, BLUE);
                    
                    
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    DrawTextEx(PongFont, "Press Enter to Play againts AI", (Vector2){screenWidth/4, screenHeight/4*2}, 50, 0, BLUE);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    ClearBackground(DARKBLUE);
                    // TODO: Draw player and enemy...................(0.2p)
           
                    DrawRectangle(player.x, player.y, rectangleWidth, rectangleHeight, (Color){ 0, 121, 241, 255 }  );
                    DrawRectangle(enemy.x, enemy.y, rectangleWidth, rectangleHeight, (Color){ 0, 121, 241, 255 }  );
                    
                    DrawCircle(ballPosition.x, ballPosition.y, ballRadius, (Color){ 24, 101, 245, 255 }  );
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                        
                    // TODO: Draw time counter.......................(0.5p)
                    text = FormatText ("%i", secondsCounter);
                    DrawTextEx(PongFont, text, (Vector2) {screenWidth/2, screenHeight/10 }, 50, 0, RED);
                    
                   
                    
                    // TODO: Draw pause message when required........(0.5p)
                   if (pause == true && (playerLife != 0 && enemyLife != 0)){
                   DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 0, 0, 100 }); 
                       
                   DrawTextEx(PongFont, "PAUSE", (Vector2) {screenWidth/2 , screenHeight/4 }, 80, 0, WHITE);
                  
                   DrawTextEx(PongFont, "Press R to restart", (Vector2) {screenWidth/2 , screenHeight/4*3 }, 50, 0, WHITE);                
                   }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    ClearBackground(DARKBLUE);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (gameResult == 0){
                    
                    DrawRectangle(0, 0, screenWidth, screenHeight, DARKBLUE); 
                    
                    DrawTextEx(PongFont, "You Lose", (Vector2) {screenWidth/2 , screenHeight/8 }, 50, 0, WHITE); 
                   
                    DrawTextEx(PongFont, "Press R to start a new game", (Vector2) {screenWidth/4, screenHeight/8*2 }, 50, 0, WHITE);
                    
                    DrawTextEx(PongFont, "Press SPACE to go to the main menu", (Vector2) {screenWidth/4 , screenHeight/8*4}, 50, 0, WHITE);    
                    
                    DrawTextEx(PongFont, "Press ESC to exit the game", (Vector2) {screenWidth/4 , screenHeight/8*6}, 50, 0, WHITE);    
                    }
                    
                    if (gameResult == 1){
                    
                    DrawRectangle(0, 0, screenWidth, screenHeight, DARKBLUE); 
                   
                    DrawTextEx(PongFont, "You win", (Vector2) {screenWidth/4, screenHeight/8 }, 50, 0, WHITE);
                    
                    DrawTextEx(PongFont, "Press R to star a new game", (Vector2) {screenWidth/4 , screenHeight/8*2}, 50, 0, WHITE);    

                    DrawTextEx(PongFont, "Press SPACE to go to the main menu", (Vector2) {screenWidth/4 , screenHeight/8*4}, 50, 0, WHITE);    
                    
                    DrawTextEx(PongFont, "Press ESC to exit the game", (Vector2) {screenWidth/4 , screenHeight/8*6}, 50, 0, WHITE);    
            
                    }
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadFont(PongFont);
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}
